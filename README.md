# Form Validation Exercise

## Quickstart

### Step One - Install Firebase CLI
[Click Here To Install Firebase CLI](https://firebase.google.com/docs/cli#install_the_firebase_cli)

### Step Two - Login to firebase through CLI

type the following command in: 
`firebase login`, then wait for firebase to propt you to login

### Step Three - Initialize project

cd to the folder where the index file is and type the following command:

### `firebase init`
 During project initialization, from the Firebase CLI prompts:

1. ### Select to set up Hosting.

   If you want to set up other Firebase products for your project, refer to their documentation for setup information. Note that you can always run firebase init later to set up more Firebase products.

2. ### Select a Firebase project to connect to your local project directory.

   The selected Firebase project is your "default" Firebase project for your local project directory. To connect additional Firebase projects to your local project directory, set up project aliases.

3. ### Specify a directory to use as your public root directory.

This directory contains all your publicly served static files, including your index.html file and any other assets that you want to deploy to Firebase Hosting.

The default for the public root directory is called public.

You can specify your public root directory now or you can specify it later in your firebase.json configuration file.

If you select the default and don't already have a directory called public, Firebase creates it for you.

If you don't already have a valid index.html file or 404.html file in your public root directory, Firebase creates them for you.

### Step Four: Deploy to your site

`firebase deploy` 
Note: Everytime you make changes and you want them to reflect on the server, you have to use this command.


## To View your site, use the link provided by firebase.

##### Karim Amin